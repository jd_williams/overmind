import overmind
import yaml

def main():
    """
    Testing and debug application...
    """
    config = None
    with open("config.yml", "r") as config_file:
        config = yaml.safe_load(config_file)

    print("Initializing providers...")
    providers = {}
    providers_config = config["providers"]
    for name,config in providers_config.items():
        print("Initializing provider {0} of type {1}".format(name, config["type"]))
        provider = _load_provider(name, config)
        providers[name] = provider
    
    search_string = "preprime"

    for name,p in providers.items():
        print("Searching {0} for {1}".format(name, search_string))
        response = p.search_for_application(search_string)
        print response.result_list

def _load_provider(name, config):
    """
    Attempt at python reflection, this should be created
    as a static method in the overmind module...
    """
    provider_type = config["type"]
    path = provider_type.split(".")
    __import__("{0}.{1}".format(path[0], path[1]))

    module_name = path[1]
    class_name = path[2]
    provider_module = getattr(overmind, module_name)
    provider_class = getattr(provider_module, class_name)

    return provider_class(name, config)

main()
