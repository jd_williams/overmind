"""
Overmind is a module which provides functions to search, describe, and restart
applications over a number of platforms. Platforms can be Octopus Deploy, ECS, 
Kubernetes, etc. 

A config.yml file provides the configuration for each provider, and configuration
can be passed in via environment variables to assist with docker container suport
"""

import requests


class ApplicationProvider(object):
    """
    Application provider is an abstraction that represents a platform. Amazon ECS, Kubernetes,
    or Windows Apps via. Octopus Deploy
    """
    def __init__(self, name=None, provider_configuration=None):
        """
        Constructor which will set the name and configure the variables
        """
        self.name = name
        self.config = provider_configuration


    def search_for_application(self, search_string):
        """
        Searches for a application and lists the application key, description, and platform
        """

        raise NotImplementedError("Search is not implemented by this provider")


    def describe_application(self, application_key):
        """
        Lists returns an application, which contains the name, description, and key. As well as
        the platform and location where the application is currently running
        """

        raise NotImplementedError("Describe is not implemented by this provider")


    def recycle_application(self, application_key, environment):
        """
        Recycle the application. This should be done in a non descructive manner.
        """
        raise NotImplementedError("Recycle is not implemented by this provider")


class BaseResponse(object):
    """
    Base response value object that will contain basic status info
    """
    def __init__(self):
        self.success = None
        self.message = None


class SearchResponse(BaseResponse):
    """
    Value object that will contain the results of a search
    """
    def __init__(self):
        super(SearchResponse, self).__init__()
        self.result_list = None


