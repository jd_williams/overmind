import boto3
import overmind
import jmespath
import json

class EcsApplicationProvider(overmind.ApplicationProvider):
    """
    This class provides an interface into ECS hosted applications
    """

    def __init__(self, name=None, provider_configuration=None):
        self.name = name
        self.aws_access_key_id = provider_configuration["aws_access_key_id"]
        self.aws_secret_access_key = provider_configuration["aws_secret_access_key"]
        self.aws_region = provider_configuration["aws_region"]
        
        # create boto3 session
        self.boto_session = boto3.Session(
            aws_access_key_id=self.aws_access_key_id,
            aws_secret_access_key=self.aws_secret_access_key,
            region_name=self.aws_region
        )

    def recycle_application(self, application_key):
        """
        This will capture current tasks of a running application, then terminate them
        one at a time. Allowing the service to become stable before terminating the 
        next task. 
    
        """
        ecs = self.boto_session.client("ecs")

        clusters = self._get_clusters()
        for cluster in clusters:
            tasks = ecs.list_tasks(
                cluster=cluster,
                serviceName=application_key
            )

            for task in tasks["taskArns"]:
                ecs.stop_task(
                    cluster=cluster,
                    task=task,
                    reason="recycle requested by user"
                )

                waiter = ecs.get_waiter("services_stable")
                waiter.wait(
                    cluster=cluster,
                    services=[application_key],
                    WaiterConfig={
                        "Delay": 10,
                        "MaxAttempts": 30
                    }
                )


    def search_for_application(self, search_string):
        """
        Searches all clusters for services whose name contains the given
        search string

        :search_string 
        """
        application_keys = []
        ecs = self.boto_session.client("ecs")
        cluster_arns = self._get_clusters()
        page_config = {
            "PageSize": 10
        }
        for cluster_arn in cluster_arns:
            service_paginator = ecs.get_paginator('list_services')

            for services in service_paginator.paginate(cluster=cluster_arn, PaginationConfig=page_config):
                services_response = ecs.describe_services(
                    cluster=cluster_arn,
                    services=services["serviceArns"]
                )
                
                if "services" in services_response:
                    for service in services_response["services"]:
                        if search_string in service["serviceName"]:
                            application_keys.append(service["serviceName"])                      

        search_response = overmind.SearchResponse()
        if application_keys:
            search_response.success = True
            search_response.result_list = application_keys
        else:
            search_response.success = False
            search_response.message = "No matching services found in this provider. :("
        
        return search_response

    def describe_application(self, application_key):
        """
        Will return metadata about the application, the current state, servers on which
        it is running. Last event in the event log
        """
        ecs = self.boto_session.client("ecs")
        clusters = self._get_clusters()
        for cluster in clusters:
            service_response = ecs.describe_services(
                cluster=cluster,
                services=[application_key]
            )

            if "services" in service_response:
                # we are only pulling a single service, so this array indexing may appear hacky
                service = service_response["services"][0]
                status = service["status"]
                desired_count = service["desiredCount"]
                running_count = service["runningCount"]
                pending_count = service["pendingCount"]
                status_string = "Desired: {0}, Running: {1}, Pending: {2}".format(
                    desired_count, 
                    running_count,
                    pending_count
                )

# private instance methods
    def _get_clusters(self):
        """
        Returns a list of clusterArns tuples
        """
        cluster_arns = []
        ecs = self.boto_session.client("ecs")
        paginator = ecs.get_paginator("list_clusters")
        
        for result in paginator.paginate():
            cluster_arns = cluster_arns + result["clusterArns"]
        
        return cluster_arns

