"""
Implementation module of application provider for Octopus Deploy
"""
import overmind
import requests

class OctopusApplicationProvider(overmind.ApplicationProvider):
    """
    This class is an ApplicationProvider for Microsoft Windows applications deployed via
    the Octopus Deploy tool.
    """
    def __init__(self, name=None, provider_configuration=None):
        self.name = name
        self.octopus_api_key = provider_configuration["octopus.api.key"]
        self.octopus_server_url = provider_configuration["octopus.server.url"]
    
    def describe_application(self, application_key):
        octopus_env_url = self.octopus_server_url + '/api/environments'

        headers = {'X-Octopus-ApiKey':self.octopus_api_key}

        environment_response = requests.get(octopus_env_url, headers = headers)
        environment_response_json = environment_response.json()

        environments = environment_response_json['Items']

        has_result = False

        for environment in environments:
            name = environment['Name']
            
            octopus_machines_url = self.octopus_server_url + environment['Links']['Machines']
            octopus_machines_url = octopus_machines_url.split('{')[0]
            machines_response = requests.get(octopus_machines_url, headers = headers)
            machines_response_json = machines_response.json()

            machine_description = ''
            has_role = False
            machines = machines_response_json['Items']

            for machine in machines:
                roles = machine['Roles']
                if application_key in roles:
                    has_role = True
                    machine_description = machine_description + "{0:40} {1}".format(machine['Name'], machine['Status']) + '\n'
                    has_result = True

            if has_role:
                print(name)
                print('')
                print (machine_description)

        if has_result is False:
            print('No applications found :(')        

    def seach_for_application(self, search_string):
        response = overmind.SearchResponse()

        octopus_roles_url = self.octopus_server_url + '/api/machineroles/all'

        headers = {'X-Octopus-ApiKey': self.octopus_api_key}

        roles_response = requests.get(octopus_roles_url, headers = headers)
        roles_response_json = roles_response.json()

        if roles_response_json:
            matching = [s for s in roles_response_json if search_string in s]
            sorted_roles = sorted(matching)
            for sorted_role in sorted_roles:
                print(sorted_role)

            if matching:
                print ('No applications found matching your search.')
            else:
                print ('No applications found matching your search.')
